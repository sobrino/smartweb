# Smart Web Project #

Web project presented in Smart Weekend Coruña in June 2014 

## Description ##

System can...

## Development Environment ##

Rails 4.1
Postgres 9.2
jQuery 10.2

### Installation ###

createdb smartweb
createuser smartweb

### Set up ###

* git clone ...
* bundle install
* rake db:migrate
* rails server
* [localhost:3000](http://localhost:3000)


## Authors ##

* Juan Carlos Alonso
* Fran Sobrino