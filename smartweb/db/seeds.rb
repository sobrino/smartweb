# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



user_1 = User.create(email: 'fransobrino+1@gmx.com', password: 'fransobrino', name: 'Josefina Martínez')
user_2 = User.create(email: 'fransobrino+2@gmx.com', password: 'fransobrino', name: 'Fermin Salvador')
user_3 = User.create(email: 'fransobrino+3@gmx.com', password: 'fransobrino', name: 'Álvaro Menendez')
user_4 = User.create(email: 'fransobrino+4@gmx.com', password: 'fransobrino', name: 'Benedicto García')
user_5 = User.create(email: 'fransobrino+5@gmx.com', password: 'fransobrino', name: 'Jesús Gayoso')
user_6 = User.create(email: 'fransobrino+6@gmx.com', password: 'fransobrino', name: 'Salvador Alegayo')

course_1 = Course.create(name: 'Pensamiento algorítmico', 
                         date: Time.now - 3.days, teacher: user_1, limit: 100, hidden: false, price: 1000, 
                         short_description: 'Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones.',
                         short_description: 'Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones.',
                         enroll_since: Time.now - 5.days,
                         enroll_till: Time.now - 3.day)
course_1.users << user_2
course_1.users << user_3

coruse_1_lecture_1 = Lecture.create(course: course_1, title: '¿que és un algoritmo?', start_at: Time.now - 3.days, duration: 40)
coruse_1_lecture_2 = Lecture.create(course: course_1, title: '¿que és un algoritmo 2?', start_at: Time.now - 2.days, duration: 40)
coruse_1_lecture_3 = Lecture.create(course: course_1, title: '¿que és un algoritmo 3?', start_at: Time.now - 1.days, duration: 45)
coruse_1_lecture_4 = Lecture.create(course: course_1, title: '¿que és un algoritmo 3?', start_at: Time.now + 1.day, duration: 50)


course_2 = Course.create(name: 'Pensamiento político', 
                         date: Time.now, teacher: user_2, limit: 100, hidden: false, price: 1000, 
                         short_description: 'Cómo desarrollar el pepensamientonsamiento algorítmico y aplicarlo en diferentes situaciones.',
                         description: 'Cómo desarrollar el penspensamientovvpensamientoamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones.',
                         enroll_since: Time.now - 1.day,
                         enroll_till: Time.now + 2.day)
course_2.users << user_3
course_2.users << user_4


course_3 = Course.create(name: 'Pensamiento anarquista', 
                         date: Time.now, teacher: user_3, limit: 100, hidden: false, price: 0, 
                         short_description: 'Cómo desarrollar el penpensamientovpensamientosamiento algorítmico y aplicarlo en diferentes situaciones.',
                         description: 'Cómo desarrollar el penpensamientopensamientovpensamientosamienpensamientopensamientopensamientopensamientovpensamientoto algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones.',
                         enroll_since: Time.now,
                         enroll_till: Time.now + 2.days)
course_3.users << user_4

course_4 = Course.create(name: 'alquimia aplicada', 
                  
                         date: Time.now, teacher: user_4, limit: 100, hidden: false, price: 1000, 
                         short_description: 'Cómo desarrollar el pensamiento algorítmico pensamientopensamientopensamientopensamientopensamientopensamientopensamientopensamientopensamientovpensamientoy aplicarlo en diferentes situaciones.',
                         description: 'Cómo desarrollar el pensamiento algorítmico ypensamientopensamiento aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones.',
                         enroll_since: Time.now + 1.day,
                         enroll_till: Time.now + 3.days)


course_demand_1 = CourseDemand.create(name: 'Como se plancha', 
                         categories: [:tecnology, :arts],
                         description: 'Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones.')

course_demand_2 = CourseDemand.create(name: 'Quiero aprender a programar', 
                         categories: [:healt, :design],
                         description: 'Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones')

resolve_course_5 = Course.create(name: 'Así se plancha', 
                         categories: [:healt],
                         date: Time.now, teacher: user_6, 
                         short_description: 'Cómo desarrollar el pensamiento algorítmico pensamientopensamientopensamientopensamientopensamientopensamientopensamientopensamientopensamientovpensamientoy aplicarlo en diferentes situaciones.',
                         description: 'Cómo desarrollar el pensamiento algorítmico ypensamientopensamiento aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo Cómo desarrollar el pensamiento algorítmico y aplicarlo en diferentes situaciones.')

resolve_course_5.users << user_5

