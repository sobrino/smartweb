# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140608094128) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "uuid-ossp"

  create_table "course_demands", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "workflow_state"
    t.string   "slug"
    t.text     "categories"
  end

  add_index "course_demands", ["user_id"], name: "index_course_demands_on_user_id", using: :btree

  create_table "courses", force: true do |t|
    t.string   "name"
    t.datetime "date"
    t.integer  "limit"
    t.boolean  "hidden"
    t.integer  "teacher_id"
    t.integer  "price"
    t.text     "short_description"
    t.text     "description",       default: "",    null: false
    t.string   "workflow_state"
    t.string   "cover"
    t.datetime "enroll_since"
    t.datetime "enroll_till"
    t.string   "slug"
    t.text     "categories"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "course_demand_id"
    t.boolean  "reported",          default: false
  end

  add_index "courses", ["course_demand_id"], name: "index_courses_on_course_demand_id", using: :btree
  add_index "courses", ["slug"], name: "index_courses_on_slug", unique: true, using: :btree

  create_table "courses_recomendations", id: false, force: true do |t|
    t.integer "course_id"
    t.integer "recomendation_id"
  end

  create_table "lectures", force: true do |t|
    t.string   "title"
    t.datetime "start_at"
    t.integer  "duration"
    t.integer  "course_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.text     "contents"
  end

  create_table "payments", force: true do |t|
    t.integer  "student_id"
    t.integer  "amount"
    t.integer  "transaction_number"
    t.boolean  "valid",              default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payments", ["student_id"], name: "index_payments_on_student_id", using: :btree

  create_table "recomendations", force: true do |t|
    t.integer  "from_user_id"
    t.integer  "to_user_id"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "recomendations", ["from_user_id"], name: "index_recomendations_on_from_user_id", using: :btree
  add_index "recomendations", ["to_user_id"], name: "index_recomendations_on_to_user_id", using: :btree

  create_table "students", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "course_id"
    t.string   "workflow_state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "students", ["course_id"], name: "index_students_on_course_id", using: :btree
  add_index "students", ["user_id"], name: "index_students_on_user_id", using: :btree

  create_table "supplies", force: true do |t|
    t.integer  "course_id"
    t.string   "name"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "supplies", ["course_id"], name: "index_supplies_on_course_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar"
    t.string   "name"
    t.string   "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
