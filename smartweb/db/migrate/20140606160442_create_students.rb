class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.references :user, index: true
      t.references :course, index: true
      t.string :workflow_state

      t.timestamps
    end
  end
end
