class CreateCoursesRecomendationsJoinTable < ActiveRecord::Migration
  def change
    create_table :courses_recomendations, id: false do |t|
      t.integer :course_id
      t.integer :recomendation_id
    end
  end
end
