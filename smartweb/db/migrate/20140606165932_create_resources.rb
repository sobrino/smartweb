class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.references :course, index: true
      t.string :name
      t.string :file

      t.timestamps
    end
  end
end
