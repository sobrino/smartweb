class AddCourseDemandToCourse < ActiveRecord::Migration
  def change
    add_reference :courses, :course_demand, index: true
  end
end
