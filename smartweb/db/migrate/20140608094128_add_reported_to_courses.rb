class AddReportedToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :reported, :boolean, default: false
  end
end
