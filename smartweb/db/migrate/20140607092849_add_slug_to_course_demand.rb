class AddSlugToCourseDemand < ActiveRecord::Migration
  def change
    add_column :course_demands, :slug, :string
  end
end
