class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.timestamp :date
      t.integer :limit
      t.boolean :hidden
      t.references :teacher
      t.integer :price
      t.text :short_description
      t.text :description
      t.string :workflow_state
      t.string :cover
      t.datetime :enroll_since
      t.datetime :enroll_till
      t.string :slug
      t.text   :description, null: false, default: ""
      t.text   :categories
      t.timestamps
    end
    add_index :courses, :slug, unique: true
  end
end
