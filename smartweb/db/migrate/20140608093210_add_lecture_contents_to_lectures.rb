class AddLectureContentsToLectures < ActiveRecord::Migration
  def change
    add_column :lectures, :contents, :text
  end
end
