class ChangeResourcesTableToSupplies < ActiveRecord::Migration
  def change
    rename_table :resources, :supplies
  end
end
