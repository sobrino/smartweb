class CreateLectures < ActiveRecord::Migration
  def change
    create_table :lectures do |t|
      t.string :title
      t.datetime :start_at
      t.integer :duration
      t.references :course
      
      t.timestamps
    end
  end
end
