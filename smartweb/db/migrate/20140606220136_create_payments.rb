class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :student, index: true
      t.integer :amount
      t.integer :transaction_number
      t.boolean :valid, default: false

      t.timestamps
    end
  end
end
