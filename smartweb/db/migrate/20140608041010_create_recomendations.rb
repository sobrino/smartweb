class CreateRecomendations < ActiveRecord::Migration
  def change
    create_table :recomendations do |t|
      t.references :from_user, index: true
      t.references :to_user, index: true
      t.text :message

      t.timestamps
    end
  end
end
