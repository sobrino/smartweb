class AddWorkflowStateToCourseDemand < ActiveRecord::Migration
  def change
    add_column :course_demands, :workflow_state, :string
  end
end
