class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy, :resolve, :report]
  before_action :authenticate_user!, except: [:index, :show]

  layout false

  def index
    @courses = can?(:manage, Course) ? Course.all : Course.visible
    if params[:filter].present?
      @courses = @courses.where("LOWER(courses.name) LIKE '%#{params[:filter].downcase}%'")
    end
    
    @courses = @courses.order(date: :desc).page(params[:page])

    render layout: 'application'
  end

  def manage
    @courses = can?(:manage, Course) ? Course.all : Course.mine(current_user)
    render layout: 'application'
  end

  def show
    render layout: 'application'
  end

  def new 
    @course = Course.new
    @course.course_demand_id = params[:course_demand_id] if params[:course_demand_id] && CourseDemand.find(params[:course_demand_id]).present?
    render :edit   
  end

  def create
    @course = Course.new(course_params)
    @course.teacher = current_user
    if @course.save
      redirect_to @course, notice: t('controllers.courses.create.success')
    else
      render action: :edit
    end
  end

  def edit 
  end

  def report
    @course.reported = true;
    @course.save
    redirect_to @course, notice: 'Hecho con éxito'
  end

  def update
    if @course.update(course_params)
      redirect_to root_path, notice: t('controllers.courses.update.success') 
    else
      render action: :edit
    end
  end


  def resolve
    @course_demand = CourseDemand.find(params[:course_demand][:course_demand_id])
    @course.course_demand = @course_demand
    @course.save!
    redirect_to @course, notice: t('controllers.course_demands.update.success') 
  end

  def destroy
    @course.destroy
    redirect_to root_path, notice: t('controllers.courses.destroy.success')
  end

  private
    def set_course
      @course = Course.friendly.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:name, :description, :short_description, :price, :cover, :course_demand_id, :hidden, :date, categories:[])
    end
end
