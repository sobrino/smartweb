class RecomendationsController < ApplicationController

  def index
    @received = Recomendation.where(from_user: current_user)
    @sent = Recomendation.where(to_user: current_user)
  end
end
