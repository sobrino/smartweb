class RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user!

  def show
    if current_user
      @user = current_user
      @students = @user.students
    else
      redirect_to root_path
    end
  end
end