class LecturesController < ApplicationController
  before_filter :authenticate_user!

  before_action :set_course
  before_action :set_lecture, only: [:show, :edit, :update, :destroy]

  def show
    authorize! :show, @lecture
  end


  def new 
    @lecture = Lecture.new
    render :edit, layout: false  
  end

  def create
    @lecture = Lecture.new(post_params)
    @lecture.course = @course
    
    if @lecture.save
      redirect_to course_lecture_path(@course,@lecture), notice: t('controllers.lectures.create.success')
    else
      render action: :edit
    end
  end


  private
    
    def set_course
      @course = Course.friendly.find(params[:course_id])
    end

    def set_lecture
      @lecture = Lecture.friendly.find(params[:id])
    end

    def post_params
      params.require(:lecture).permit(:title, :start_at, :duration, )
    end
    
end
