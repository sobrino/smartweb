class StudentsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_course
  before_action :set_student, only: [:validate, :destroy]

  def index
    authorize! :update, @course
    @students = @course.students
  end

  def create
    @student = Student.new(user: current_user, course: @course, name: current_user.name)
    if @student.save 
      path = @course.free? ? course_path(@course) : course_payments_path(@course)
      redirect_to path, notice: t('controllers.courses.create.success')
    else
      redirect_to root_path, error: t('controllers.courses.create.error')
    end
  end

  def validate
    authorize! :update, @course
    @student.paid!
    redirect_to @course
  end

  def destroy
    @student.destroy
    redirect_to root_path, notice: t('controllers.courses.destroy.success')
  end

  private
    
    def set_course
      @course = Course.friendly.find(params[:course_id])
    end

    def set_student
      @student = Student.find(params[:id])
    end

    def checkpoint_params
      params.require(:checkpoint).permit()
    end
end
