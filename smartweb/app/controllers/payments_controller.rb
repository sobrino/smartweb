class PaymentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_course

  def index
  end

  def validate
  end

   private
    
    def set_course
      @course = Course.friendly.find(params[:course_id])
    end


end
