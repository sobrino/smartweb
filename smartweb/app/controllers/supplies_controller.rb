class SuppliesController < ApplicationController

   before_filter :authenticate_user!

  before_action :set_course
  before_action :set_supply, only: [:show, :edit, :update, :destroy]

  def show
    authorize! :read, @supply
  end


 def new 
    @supply = Supply.new
    render :edit   
  end

  def create
    @supply = Supply.new(post_params)
    @supply.course = @course
    
    if @supply.save
      redirect_to course_supply_path(@course,@supply), notice: t('controllers.supplys.create.success')
    else
      render action: :edit
    end
  end


  private
    
    def set_course
      @course = Course.friendly.find(params[:course_id])
    end

    def set_supply
      @supply = Supply.friendly.find(params[:id])
    end

    def post_params
      params.require(:supply).permit(:title, :start_at, :duration)
    end


end
