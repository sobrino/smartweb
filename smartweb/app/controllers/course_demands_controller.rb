class CourseDemandsController < ApplicationController
  before_action :set_course_demand, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  layout false

  def index
    @course_demands = CourseDemand.all 
    if params[:filter].present?
      @course_demands = @course_demands.where("LOWER(course_demands.name) LIKE %#{params[:filter].downcase}%")
    end
    
    @course_demands = @course_demands.order(created_at: :desc).page(params[:page])

    render layout: 'application'
  end

  def manage
    @course_demands = CourseDemand.all
    render layout: 'application'
  end

  def show
    render layout: 'application'
  end

  def new 
    @course_demand = CourseDemand.new
    render :edit   
  end

  def create
    @course_demand = CourseDemand.new(course_demand_params)
    @course_demand.user = current_user
    if @course_demand.save
      redirect_to root_path, notice: t('controllers.course_demands.create.success')
    else
      render action: :edit
    end
  end

  def edit 
  end

  def update
    if @course_demand.update(course_demand_params)
      redirect_to root_path, notice: t('controllers.course_demands.update.success') 
    else
      render action: :edit
    end
  end

  def destroy
    @course_demand.destroy
    redirect_to root_path, notice: t('controllers.course_demands.destroy.success')
  end

  private
    def set_course_demand
      @course_demand = CourseDemand.friendly.find(params[:id])
    end

    def course_demand_params
      params.require(:course_demand).permit(:name, :description, :short_description, :cover, categories:[])
    end
end
