class UsersController < ApplicationController

  def manage_roles
    authorize! :manage, User
    @user = User.find(params[:id])
    @user.role = params[:user][:role]
    @user.save!
  end

end