object @course

attributes :id => :course_id
attributes :name, :date, :description, :short_description

node(:minutes) {|course| course.minutes}
node(:students) {|course| course.students.count}


child :lectures do 
  extends 'lectures/base'
end