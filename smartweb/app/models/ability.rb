class Ability
  include CanCan::Ability

  def initialize(user)
   
    cannot :manage, :all



    if user
      can [:create], Recomendation

      if user.role == :admin
        can :manage, :all
      else
        can [:read, :create, :enroll],    Course
        cannot :enroll,          Course, teacher_id: user.id 
        can [:update, :destroy], Course, teacher_id: user.id 
        can [:show],             Course, students: { workflow_state: "enrolled", user_id: user.id }
        can [:show],             Course, teacher_id: user.id 

        can [:show],          Lecture do |lecture|
          (lecture.start_at.nil? || lecture.start_at < Time.now) &&
          user.students.where("workflow_state = 'enrolled'").pluck(:course_id).include?(lecture.course_id)
        end
         
        can [:create, :read, :update, :destroy], CourseDemand, user_id: user.id 
      end
    end


  end
end
