class Payment < ActiveRecord::Base
  belongs_to :student


  validates :transaction_number, uniqueness: true
  validates :amount, :numericality => { :greater_than => 0 }
  validates :valid, presence: true

  before_validation :set_transaction_number, on: :create



  private
    def set_transaction_number
      self.transaction_number = student.id.to_s + '0' + Time.now.to_i.to_s 
    end

end
