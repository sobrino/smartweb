class CourseDemand < ActiveRecord::Base

  belongs_to :user

  has_many :courses, dependent: :nullify
  
  scope :open, -> {where.not(workflow_state: "resolved")}

  #Enumerize -> categories
  extend Enumerize
  serialize :categories, Array
  enumerize :categories, in: [:tecnology, :arts, :healt, :design], multiple: true

  #FriendlyId -> name and slug
  extend FriendlyId
  friendly_id :name, use: :slugged

  # Workflow
  include Workflow
  workflow do
    state :publish do
      event :resolve, :transitions_to => :resolved
    end
    state :resolved 
  end

end
