class Lecture < ActiveRecord::Base
  belongs_to :course


  validates :course, presence: true
  #FriendlyId -> title and slug
  extend FriendlyId
  friendly_id :title, use: :slugged


end
