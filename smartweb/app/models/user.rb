class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :avatar, AvatarUploader

  has_many :students, dependent: :destroy
  has_many :course_demands, dependent: :destroy
  has_many :courses, foreign_key: :teacher
  has_and_belongs_to_many :courses, join_table: 'students'

  #Enumerize -> categories
  extend Enumerize
  enumerize :role, in: [:admin]
  
end
