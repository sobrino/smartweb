class Course < ActiveRecord::Base

  belongs_to :teacher, class_name: 'User'
  belongs_to :course_demand

  has_many :students, dependent: :destroy
  has_many :supplies, dependent: :destroy
  has_many :lectures, dependent: :destroy
  has_and_belongs_to_many :users, join_table: :students

  validates :name, presence: true
  validates :teacher, presence: true
  validates :short_description, presence: true

  scope :visible, -> {where.not(hidden: true)}

  #Carrierwave -> cover
  mount_uploader :cover, CoverUploader

  #Enumerize -> categories
  extend Enumerize
  serialize :categories, Array
  enumerize :categories, in: [:tecnology, :arts, :healt, :design], multiple: true

  #FriendlyId -> name and slug
  extend FriendlyId
  friendly_id :name, use: :slugged

  # Workflow
  include Workflow
  workflow do
    state :publish do
      event :start, :transitions_to => :ongoing
    end
    state :ongoing do
      event :finish, :transitions_to => :finished
    end
    state :finished 
  end

  #methods
  def minutes
    self.lectures.sum(:duration)
  end

  def self.mine user 
    user ? Course.where(teacher_id: user.id) : []
  end

  def free?
    self.price == 0
  end

  def student_by user
    Student.where(course_id: self.id, user_id: user.id).first  
  end
end
