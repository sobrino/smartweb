class Recomendation < ActiveRecord::Base
  belongs_to :from_user, class_name: 'User'
  belongs_to :to_user, class_name: 'User'

  has_and_belongs_to_many :courses_recomendations

  validates :to_user, presence: true
end
