class Student < ActiveRecord::Base
  belongs_to :user
  belongs_to :course

  has_many :payments
  
  validates :user, uniqueness: { scope: :course, allow_nil: false}
  validates :course, presence: true
  
  scope :valid, -> {where(workflow_state: "enrolled")}

  # Workflow
  include Workflow
  workflow do
    state :pending_approbal do
      event :paid, transitions_to: :enrolled
      event :accept, transitions_to: :enrolled
    end
    state :enrolled
  end

end
