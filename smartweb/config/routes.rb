Rails.application.routes.draw do

  devise_for :users
  as :user do
  get 'profile' => 'registrations#show'
end

  root to: "pages#index"
  resources :courses do
    get 'manage', on: :collection
    post 'resolve', on: :member
    post 'report', on: :member
    resources :payments do
      post 'validate', on: :collection
    end
    
    resources :students do
      post 'validate', on: :member
    end
    resources :lectures
    resources :supplies
  end

  resources :course_demands do 
    get 'manage', on: :collection
  end


  resources :recomendations


end
